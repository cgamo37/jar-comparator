package my.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import difflib.Delta;
import difflib.DiffUtils;

/**
 * 
 * @author cesar CGM start on ju vi 24 y 25 de marzo de 2016 vacaciones de
 *         semana santa.
 *
 */
@SuppressWarnings({ "PMD.LawOfDemeter", "PMD.DataflowAnomalyAnalysis", "PMD.CommentSize" })
public final class JarComparator {

	/** Standard logger */
	private static final Logger LOGGER = LoggerFactory.getLogger(JarComparator.class);

	/** PMD requires it */
	private JarComparator() {
		// To PMD
	}

	/**
	 * Compares two files.
	 * 
	 * @param pathFile1
	 *            path to file #1.
	 * @param pathFile2
	 *            path to file #2.
	 * @return List of differences between jar files.
	 * @throws IOException
	 *             if no exist some of the files.
	 */
	public static List<Delta<String>> compareFiles(final String pathFile1, final String pathFile2) throws IOException {
		final Map<String, String> jar1decompiled = new JarDecompiler(pathFile1).decompile(); // NOPMD UseConcurrentHashMap
		final Map<String, String> jar2decompiled = new JarDecompiler(pathFile2).decompile(); // NOPMD UseConcurrentHashMap
		final List<Delta<String>> diffs = new ArrayList<>();
		for (final Entry<String, String> entry : jar1decompiled.entrySet()) {
			final String key = entry.getKey();
			if (jar2decompiled.containsKey(key)) {
				// both jars have this class: compare the sources decompiled:
				final String file1 = jar1decompiled.get(key);
				final String file2 = jar2decompiled.get(key);
				final List<String> original = Arrays.asList(file1.split("\n"));
				final List<String> revised = Arrays.asList(file2.split("\n"));
				diffs.addAll(calculateGlobalDiff(original, revised, key));
			} else {
				LOGGER.info("key '{}' is only in jar1.", key);
			}
		}
		return diffs;
	}

	private static List<Delta<String>> calculateGlobalDiff(final List<String> original, final List<String> revised,
			final String className) {
		final List<Delta<String>> deltas = DiffUtils.diff(original, revised).getDeltas();
		if (!deltas.isEmpty()) {
			final StringBuilder diffStr = new StringBuilder(200);
			diffStr.append("________________________________________________\n");
			deltas.stream().forEach((Delta<String> delta) -> {
				diffStr.append(addPositionInfo(delta))
						.append(' ').append(delta.getType()).append('\n')
						.append(getDeltaLines(delta.getOriginal().getLines()))
						.append("------------------------------------------------\n")
						.append(getDeltaLines(delta.getRevised().getLines()))
						.append("________________________________________________\n");
			});
			LOGGER.info("class: {}\ndifferences:\n{}", className, diffStr);
		}
		return deltas;
	}

	private static String addPositionInfo(final Delta<String> delta) {
		final StringBuilder positionInfo = new StringBuilder(50);
		final Integer origPos = delta.getOriginal().getPosition();
		final Integer revPos = delta.getRevised().getPosition();
		positionInfo.append(origPos).append(' ').append(revPos);
		return positionInfo.toString();
	}

	private static String getDeltaLines(final List<String> lines) {
		final StringBuilder diffStr = new StringBuilder(200);
		lines.stream().forEach(line -> {
			diffStr.append(line).append('\n');
		});
		return diffStr.toString();
	}

}
