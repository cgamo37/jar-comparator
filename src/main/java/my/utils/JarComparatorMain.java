package my.utils;

import static my.utils.JarComparator.compareFiles;

import java.io.IOException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import my.utils.exception.MissingRequiredArgumentsException;

/**
 * 
 * @author César García Mauricio - 02 june 2016
 *
 */
public final class JarComparatorMain {

	/** Standard logger */
	private static final Logger LOGGER = LoggerFactory.getLogger(JarComparatorMain.class);

	/** PMD requires it */
	private JarComparatorMain() {
		// To PMD
	}

	/**
	 * CLI interface to application.
	 * 
	 * @param args
	 *            first entry is path to file1; second entry, path to file2.
	 * @throws IOException
	 */
	@SuppressWarnings("PMD.LawOfDemeter")
	public static void main(final String... args) throws IOException {
		final Options options = new Options();
		// options.addOption("v", false, "Application Version");
		options.addOption("f1", true, "Path to jar file #1");
		options.addOption("f2", true, "Path to jar file #2");
		try {
			final CommandLineParser parser = new DefaultParser();
			final CommandLine cmdLine = parser.parse(options, args);

			if(!cmdLine.hasOption("f1") || !cmdLine.hasOption("f2")) {
				new HelpFormatter().printHelp(JarComparatorMain.class.getCanonicalName(), options);
				throw new MissingRequiredArgumentsException();
			}

			final String pathFile1 = cmdLine.getOptionValue("f1");
			LOGGER.trace("pathFile1: '{}'", pathFile1);
			final String pathFile2 = cmdLine.getOptionValue("f2");
			LOGGER.trace("pathFile2: '{}'", pathFile2);
			compareFiles(pathFile1, pathFile2);
		} catch (final ParseException e) {
			new HelpFormatter().printHelp(JarComparatorMain.class.getCanonicalName(), options);
		}
	}

}
