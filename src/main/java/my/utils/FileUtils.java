package my.utils;

import java.io.IOException;

import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;

/**
 * 
 * @author César García Mauricio
 *
 */
public final class FileUtils {
	/** standard logger */
	private static final Logger LOGGER;
	/**
	 * Tika is a facade for several APIs used for extracting clear text from
	 * several file formats.
	 */
	private static final Tika TIKA;

	static {
		LOGGER = LoggerFactory.getLogger(FileUtils.class);
		TIKA = new Tika();
		TIKA.setMaxStringLength(-1);
	}

	private FileUtils() {
		// TO PMD
	}

	/**
	 * Reads file content as text.
	 * 
	 * @param fileName
	 *            Absolute or relative path to file.
	 * @return a string with file's contents.
	 * @throws IOException
	 */
	public static String readFile(final String fileName) throws IOException {
		LOGGER.debug("Leyendo el contenido del archivo '{}'", fileName);
		final StringBuffer fileAsString = new StringBuffer("\n");
		try {
			fileAsString.append(TIKA.parseToString(new FileSystemResource(fileName).getInputStream()));
		} catch (final TikaException e) {
			LOGGER.error("Error reading {} {}", fileName, e.getMessage());
		}
		LOGGER.trace("fileAsString:{}", fileAsString);
		return fileAsString.toString();
	}

}
