package my.utils.exception;

/**
 * 
 * @author César García Mauricio
 *
 */
@SuppressWarnings("serial")
public class MissingRequiredArgumentsException extends IllegalArgumentException {

}
