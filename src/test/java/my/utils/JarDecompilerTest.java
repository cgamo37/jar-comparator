package my.utils;

import static org.junit.Assert.assertFalse;

import java.io.IOException;

import org.junit.Test;

/**
 * 
 * @author César García Mauricio - 28 may 2016
 *
 */
@SuppressWarnings("PMD.AtLeastOneConstructor")
public class JarDecompilerTest {
	/** path to jar - file to decompile */
	private transient String jarPath;

	/**
	 * Test to decompile jar in absolute path. Case of no file in path.
	 * 
	 * @throws IOException
	 * 
	 */
	// @Test(expected = FileNotFoundException.class) TODO this exception or?
	@Test(expected = IllegalArgumentException.class)
	public final void testDecompileNoJarFile() throws IOException {
		jarPath = "F:/shared/cards/jars-test/zic_cards-facade.jar";
		new JarDecompiler(jarPath).decompile();
	}

	/**
	 * Test to decompile jar in absolute path.
	 * 
	 * @throws IOException
	 * 
	 */
	@Test
	@org.junit.Ignore("Integration test - slow and depends of existing file.")
	public final void testDecompileJarAbsolutePath() throws IOException {
		jarPath = "C:/Users/gamo__000/Dropbox/shared/projects/beyond-beyond-1.6/beyond-beyond/build/libs/beyond-beyond.jar";
		assertFalse("", new JarDecompiler(jarPath).decompile().entrySet().isEmpty());
	}

}
