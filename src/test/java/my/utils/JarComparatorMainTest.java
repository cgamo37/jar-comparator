package my.utils;

import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import difflib.Delta;
import my.utils.exception.MissingRequiredArgumentsException;

/**
 * 
 * @author César García Mauricio
 *
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(JarComparator.class)
@SuppressWarnings({ "PMD.CommentRequired", "PMD.AtLeastOneConstructor" })
public class JarComparatorMainTest {

    private static final String PATH_FILE1 = "path-to-jar_1";
    private static final String PATH_FILE2 = "path-to-jar_2";
    private static final List<Delta<String>> LIST = new ArrayList<Delta<String>>();

    @Test(expected = MissingRequiredArgumentsException.class)
    public final void testMain() throws IOException {
        JarComparatorMain.main(new String[0]);
    }

    @Test
    @SuppressWarnings("PMD.LawOfDemeter")
    public final void testMainSuccess() throws IOException {
        mockStatic(JarComparator.class);
        when(JarComparator.compareFiles(PATH_FILE1, PATH_FILE2)).thenReturn(LIST);
        // Arguments passed looks like:
        // java my.utils.JarComparatorMainTest -f1 path-to-jar_1 -f2 path-to-jar_2
        // and of course, after setting the required class-path.
        JarComparatorMain.main("-f1", PATH_FILE1, "-f2", PATH_FILE2);
        verifyStatic();
        JarComparator.compareFiles(PATH_FILE1, PATH_FILE2);
    }

}
