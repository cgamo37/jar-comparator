package my.utils;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Test;

/**
 * 
 * @author César García Mauricio
 *
 */
@SuppressWarnings("PMD.AtLeastOneConstructor")
public class JarComparatorTest {
	/** Path to file #1 */
	private transient String pathFile1;
	/** Path to file #2 */
	private transient String pathFile2;

	/**
	 * case of non-jar file.
	 * 
	 * @throws IOException
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testCompareNotJarFiles() throws IOException {
		pathFile1 = "aNonJarFile.java";
		JarComparator.compareFiles(pathFile1, pathFile2);
	}

	/**
	 * Example of usage of API.
	 * 
	 * @throws IOException
	 */
	// TODO To improve asserts
	@Test
	public void testCompareJars() throws IOException {
		pathFile1 = "src/test/resources/jars-compare/01-jar-comparator-0.0.1-SNAPSHOT.jar";
		pathFile2 = "src/test/resources/jars-compare/02-jar-comparator-0.0.1-SNAPSHOT.jar";
		assertNotNull("", JarComparator.compareFiles(pathFile1, pathFile2));
	}

}
